
# [Amazon SageMaker Documentation](https://docs.aws.amazon.com/sagemaker/index.html)
## [Pricing](https://aws.amazon.com/sagemaker/pricing/?icmpid=docs_sagemaker_lp)
## Python SDK

[SageMaker Developers Guide](https://docs.aws.amazon.com/sagemaker/latest/dg/whatis.html)

[Python SageMaker SDK](https://sagemaker.readthedocs.io/en/stable/index.html)

[Python SageMaker SDK github](https://github.com/aws/sagemaker-python-sdk)

[Python Boto3 SDK](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sagemaker.html?icmpid=docs_sagemaker_lp)

## build in algorithms
(\*) disitributed training
<\*> incremental training

### Classification
- linear learner (\*)
- XGBoost
- KNN
- Factorization Machines

### Computer Vision
- Image Classification <\*>
- Object Detection <\*>
- Semantic Segmentation

### Topic Modeling
- LDA
- NTM

### Working with Text
- Blazing Text
 - supervised
 - un-supervised (\*)

### Recommendation
- Factorization Machines (\*) + KNN

### Anomlay Detection
- Random Cut Forests (\*)
- IP Insights (\*)

### Clustering
- Kmeans (\*)
- KNN

### Sequence Translation
- Seq2Seq (\*)

### Regression
- Linear Learner
- XGBoost
- KNN

### Feature Selection
- PCA
- Object2Vec


**TODO**
- find aws git dockerfiles/ definitions for Algorithms
- find Whitepapers for Algorithms
